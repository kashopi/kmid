#include<stdio.h>
#include<conio.h>
#include<string.h>
#include"allegro.h"

volatile int x,y;
unsigned char buffer[100];
volatile long int last_pos=-10;

void actualiza_datos()
{
 if(last_pos!=midi_pos)
 {
//  gotoxy(x,y);
//  cprintf("Frame  : %lu  ",midi_pos);
 }
}

END_OF_FUNCTION(actualiza_datos);

void *my_midi_msg_callback(int msg, int byte1, int byte2)
{
// gotoxy(1,y+5);
   printf("*         Msg: %d / %d / %d\n",msg,byte1,byte2);
}
void *my_midi_meta_callback(int type, unsigned char *data,int length)
{
 strncpy(buffer,data,length);
 buffer[length]='\0';
 printf("* Meta:  %d / %s\n",type,buffer);

}
void *my_midi_sysex_callback(unsigned char *data,int length)
{
 strncpy(buffer,data,length);
 buffer[length]='\0';
// gotoxy(1,y+7);
  printf("*   SysEx:  %s\n",buffer);
}


main(int i,char *argv[])
{
    MIDI *song;
    int error=0;
    char tecla;
    int midi_en_pausa=FALSE;

    midi_msg_callback=(void *)my_midi_msg_callback;
    midi_meta_callback=(void *)my_midi_meta_callback;
    midi_sysex_callback=(void *)my_midi_sysex_callback;

    cprintf("Ksp MID player v0.1 Alpha 1998\n");

 	if(i<2)
      {
       printf("Uso: kmid [cancion.mid]\n");
       printf("Programado con Allegro v3.0");
	   exit(2);
      }

    i_love_bill=TRUE;

    reserve_voices(0,32);

    allegro_init();
    install_keyboard();
    install_timer();

   if (install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, argv[0]) != 0) {
      printf("Error: no puedo detectar el hardware (%s)\n", allegro_error);
      allegro_exit();
   }

    song=load_midi(argv[1]);

    error=play_midi(song,0);
    if(error!=0)
      {
        printf("Error: no puedo inicializar el dispositivo MIDI\n");
        allegro_exit();
      }

    printf("Tocando: %s\n",argv[1]);

    x=1;
    y=wherey();

    LOCK_FUNCTION(actualiza_datos);
    LOCK_VARIABLE(x);
    LOCK_VARIABLE(y);
    LOCK_VARIABLE(buffer);
    LOCK_VARIABLE(last_pos);
    install_int(actualiza_datos, 100);

    while(tecla!='s' && tecla!='S')
    {
      if(keypressed()==TRUE)
        {
         tecla=readkey();

         switch(tecla)
           {
            case 'p':
            case 'P':
                 {
                     if(midi_en_pausa==TRUE)
                     {
                      midi_resume();
                      midi_en_pausa=FALSE;
                     }
                     else
                     {
                      midi_pause();
                      midi_en_pausa=TRUE;
                     }
                }
            	break;
            case 'a':
            case 'A':
                 {
                    midi_seek(midi_pos+1);
                 }
            	break;
            case 'r':
            case 'R':
                 {
                    midi_seek(midi_pos-1);
                 }
            	break;
            case 'e':
            case 'E':
                 {
                    midi_seek(0);
                 }
            	break;

           default:
                break;
           }

         }
    }

    allegro_exit();
}


